﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CinemaApi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize]
  public class ReservationsController : ControllerBase
  {
    // GET: api/<ReservationsController>
    [HttpGet]
    public IEnumerable<string> Get()
    {
      return new string[] { "value1", "value2" };
    }

    // GET api/<ReservationsController>/5
    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/<ReservationsController>
    [HttpPost]
    public void Post([FromBody] string value)
    {
    }

    // DELETE api/<ReservationsController>/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
}
