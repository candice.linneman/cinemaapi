﻿using Entities;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CinemaApi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  //[Authorize]
  public class MoviesController : ControllerBase
  {
    private RepositoryContext _repository;

    public MoviesController(RepositoryContext repository)
    {
      _repository = repository;
    }

    // GET: api/<MoviesController>
    [HttpGet()]
    public IActionResult Get(string sort, int? pageSize, int? pageNumber, string keyword)
    {
      var currentPageNumber = pageNumber ?? 1;
      var currentPageSize = pageSize ?? 5;

      var movies = _repository.Movies
        .Where(m => String.IsNullOrWhiteSpace(keyword) || m.Name.StartsWith(keyword))        
        .Select(m => new 
        { 
          Id = m.Id, 
          Name = m.Name, 
          Rating = m.Rating 
        })
        .Skip((currentPageNumber - 1) * currentPageSize).Take(currentPageSize);

      switch (sort)
      {
        case "desc":
          return Ok(movies.OrderByDescending(m => m.Rating));
        case "asc":
          return Ok(movies.OrderBy(m => m.Rating));
        default:
          return Ok(movies.OrderBy(m => m.Name));
      }
    }

    // GET api/<MoviesController>/5
    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
      var movie = _repository.Movies.Find(id);
      if (movie == null)
        return NotFound("There is no record with that Id");

      return Ok(movie);
    }

    // POST api/<MoviesController>
    [HttpPost]
    [Authorize(Roles = Constants.RoleNameAdmin)]
    public IActionResult Post([FromForm] Movie movie)
    {
      var fileId = Guid.NewGuid();
      var fileName = fileId + ".jpg";
      var filePath = Path.Combine("wwwroot", fileName);
      if (movie.Image != null)
      {
        var fileStream = new FileStream(filePath, FileMode.Create);
        movie.Image.CopyTo(fileStream);

        movie.ImageUrl = fileName;
      }

      _repository.Movies.Add(movie);
      _repository.SaveChanges();
      return StatusCode(StatusCodes.Status201Created);
    }

    // PUT api/<MoviesController>/5
    [HttpPut("{id}")]
    [Authorize(Roles = Constants.RoleNameAdmin)]
    public IActionResult Put(int id, [FromForm] Movie movieObj)
    {
      var movie = _repository.Movies.Find(id);
      if (movie == null)
        return NotFound("There is no record with that Id");

      var fileId = Guid.NewGuid();
      var fileName = fileId + ".jpg";
      var filePath = Path.Combine("wwwroot", fileName);
      if (movieObj.Image != null)
      {
        var fileStream = new FileStream(filePath, FileMode.Create);
        movieObj.Image.CopyTo(fileStream);

        movie.ImageUrl = fileName;
      }

      movie.Name = movieObj.Name;
      movie.Language = movieObj.Language;
      movie.Rating = movieObj.Rating;

      _repository.SaveChanges();

      return Ok("Record updated successfully");
    }

    // DELETE api/<MoviesController>/5
    [HttpDelete("{id}")]
    [Authorize(Roles = Constants.RoleNameAdmin)]
    public IActionResult Delete(int id)
    {
      var movie = _repository.Movies.Find(id);
      if (movie == null)
        return NotFound("There is no record with that Id");

      _repository.Movies.Remove(movie);
      _repository.SaveChanges();

      return Ok("Record deleted successfully");
    }
  }
}
