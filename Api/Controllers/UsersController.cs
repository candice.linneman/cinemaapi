﻿using AuthenticationPlugin;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Entities;

namespace CinemaApi.Controllers
{
  [Route("api/[controller]/[action]")]
  [ApiController]
  public class UsersController : ControllerBase
  {
    private RepositoryContext _repository;
    private IConfiguration _configuration;
    private readonly AuthService _auth;

    public UsersController(RepositoryContext repository, IConfiguration configuration)
    {
      _repository = repository;
      _configuration = configuration;
      _auth = new AuthService(_configuration);
    }

    [HttpPost]
    public IActionResult Register([FromBody] User user)
    {
      var userExists = _repository.Users.Where(u => u.Email == user.Email).SingleOrDefault();
      if (userExists != null)
        return BadRequest("User already exists with this email");

      var newUser = new User() {
        Name = user.Name,
        Email = user.Email,
        Password = SecurePasswordHasherHelper.Hash(user.Password),
        Role = Constants.RoleNameUser
      };
      _repository.Users.Add(newUser);
      _repository.SaveChanges();

      return StatusCode(StatusCodes.Status201Created);       
    }

    [HttpPost]
    public IActionResult Login([FromBody] User user)
    {
      var userEmail = _repository.Users.FirstOrDefault(u => u.Email == user.Email);

      if (userEmail == null)
        return NotFound();

      if (!SecurePasswordHasherHelper.Verify(user.Password, userEmail.Password))
        return Unauthorized();

      var claims = new[] {
        new Claim(JwtRegisteredClaimNames.Email, user.Email),
        new Claim(ClaimTypes.Email, user.Email),
        new Claim(ClaimTypes.Role, userEmail.Role)
      };

      var token = _auth.GenerateAccessToken(claims);

      return new ObjectResult(new {
        access_token = token.AccessToken,
        expires_in = token.ExpiresIn,
        token_type = token.TokenType,
        creation_time = token.ValidFrom,
        expiration_time = token.ValidTo,
        user_Id = userEmail.Id
      });
    }
   }
}
