﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities.Models
{
  public static class Constants
  {
    public const string RoleNameAdmin = "Admin";
    public const string RoleNameUser = "User";
  }
}
